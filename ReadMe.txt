The source code is in Python 2.7

Opening
input: xxxxxxxxxWxxxxxxBxxxxxx
	MiniMax:
		MiniMaxOpening ip_file.txt op_file.txt 2
		Input board: xxxxxxxxxWxxxxxxBxxxxxx
		Board Position:   WxxxxxxxxWxxxxxxBxxxxxx
		Positions evaluated by static estimation: 442
		MINIMAX estimate: 0
	Alpha-Beta Pruning:
		ABOpening ip_file.txt op_file.txt 2
		Input Board:  xxxxxxxxxWxxxxxxBxxxxxx
		Board Position:   WxxxxxxxxWxxxxxxBxxxxxx
		Positions evaluated by static estimation: 62
		MINIMAX estimate: 0
	MiniMaxBlack:
		MiniMaxOpeningBlack ip_file.txt op_file.txt 2
		Input board: xxxxxxxxxWxxxxxxBxxxxxx
		Board Position:   BxxxxxxxxWxxxxxxBxxxxxx
		Positions evaluated by static estimation: 442
		MINIMAX estimate: 0


MidGameEndGame:
input:WWWWBWWWWWBBBBBBBBxxxxx
	MiniMax:
		MiniMaxGame.py  ip_file.txt op_file.txt 3
		Board Position:   WWWWBWWWxWBBBBBBBBxxWxx
		Positions evaluated by static estimation: 48
		MINIMAX estimate: 992
	ABGame:
		ABGame.py  ip_file.txt op_file.txt 3
		Board Position:   WWWWBWWWxWBBBBBBBBxxWxx
		Positions evaluated by static estimation: 40
		MINIMAX estimate: 1988
	MiniMaxBlack:
		MiniMaxGame ip_file.txt op_file.txt 3
		input board: WWWWBWWWWWBBBBBBBBxxxxx
		Board Position:   WWWWBWWWWWBBxBBBBBxBxxx
		Positions evaluated by static estimation: 65
		MINIMAX estimate: 996

Improved Static Estimation

Opening: if maximizer closes a mill, its estimate is reduced to the least possible value so that it will be picked in the minimizer's move.

input: WWWxxxxxxxxxxxxxxxxxBxB
					B     x     B
					  x   x   x
						x x x
					x x x 0 x x x
						x   x
					  x   x   x
					W     W     W
	MiniMaxOpening:
		MiniMaxOpening ip_file.txt op_file.txt 4
		Input board: WWWxxxxxxxxxxxxxxxxxBxB
		Board Position:   WWWWxxxxxxxxxxxxxxxxBxB
		Positions evaluated by static estimation: 79287
		MINIMAX estimate: 1
	MiniMaxOpeningImproved:
		MiniMaxOpeningImproved ip_file.txt op_file.txt 4
		Input board: WWWxxxxxxxxxxxxxxxxxBxB
		Board Position:   WWWxxxxxxxxxxxxxxxxxBWB
		Positions evaluated by static estimation: 79282
		MINIMAX estimate: 1

Input board: BxBWxWxxxBxxxBxxWBxWWWx
				W     W     x
				  B   x   W
					x x W
				x B x 0 x x B
					x   x
				  W   x   W
				B     x     B
	
	MiniMaxOpening:
		MiniMaxOpening ip_file.txt op_file.txt 4
		Input board: BxBWxWxxxBxxxBxxWBxWWWx
		Board Position:   xxBWWWxxxBxxxBxxWBxWWWx
		Positions evaluated by static estimation: 15205
		MINIMAX estimate: 3
	MiniMaxOpeningImproved:
		MiniMaxOpeningImproved ip_file.txt op_file.txt 4
		Input board: BxBWxWxxxBxxxBxxWBxWWWx
		Board Position:   xxBWxWxxxBxxxBxxWBxWWWW
		Positions evaluated by static estimation: 15186
		MINIMAX estimate: -18

MidGameEndGame: if a move by maximizer results in a mill by the minimizer's next move, the estimate of the maximizer's move is increased so that it is not chosen by the minimizer.
					x     x     x
					  B   x   x
						x x B
					B W B 0 W x x 
						B   W
					  B   x   x
					W     x     B
	MiniMaxGame:
		MiniMaxGame ip_file.txt op_file.txt 3
		input board: WxBBxxBWBWBWxxxxBBxxxxx
		Board Position:   xWBBxxBWBWBWxxxxBBxxxxx
		Positions evaluated by static estimation: 218
		MINIMAX estimate: -3013
	MiniMaxGameImproved:
		MiniMaxGameImproved ip_file.txt op_file.txt 3
		Input board: WxBBxxBWBWBWxxxxBBxxxxx
		Board Position:   WxBBxWBxBWBWxxxxBBxxxxx
		Positions evaluated by static estimation: 249
		MINIMAX estimate: -3861		
