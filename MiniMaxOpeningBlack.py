import sys
from sys import maxsize



class MiniMaxOpening():

    def __init__(self):
        self.numWhitePieces = 0 #the number of white pieces on board.
        self.numBlackPieces = 0 #the number of black pieces on board.
        self.numPieces = 0
        self.W = 'W'
        self.B = 'B'
        L = [] # the MidgameEndgame positions generated from b by a black move.
        # numBlackMoves = the number of board positions
        self.positions_evaluated_by_static_estimation = 0
        self.bestMove = []
        
    def readInput(self,ip_file, op_file):
        try:
            # print('reading input positions from ip_file.txt')
            with open(ip_file, 'r') as ipf:
                board = ipf.readlines()
                board = [list(x.strip('\n'))  for x in board if len(x) >=23]                
            return board[0]
        except Exception as e:
            print(str(e))
        finally:
            ipf.close()

    def generateMovesOpening(self,board,player):
        L = self.generateAdd(board,player)
        return L

    def generateAdd(self,board,player):
        L = []
        for i in range(23):
            if board[i] == 'x':
                b = board[:]
                b[i] = player
                if self.closeMill(i,b):
                    self.generateRemove(b,L,self.W  if player==self.B else self.B)
                else:
                    L.append(b)
        return L
    
    def countWB(self,board):
            self.numBlackPieces = 0
            self.numWhitePieces = 0
            for i in range(23):
                if board[i] == 'W':
                    self.numWhitePieces += 1
                elif board[i] == 'B':
                    self.numBlackPieces += 1
    
    def generateMovesMidgameEndgame(self,board,player):
        # print('generateMovesMidgameEndgame')
        self.countWB(board)
        if player == self.W:
            self.numPieces = self.numWhitePieces
        else:
            self.numPieces = self.numBlackPieces
        if self.numPieces == 3:
            return self.generateHopping(board,player)
        else:
            return self.generateMove(board,player)
        
    def generateMove(self,board,player):
        # print('generateMove')
        L = []
        for pos in range(23):
            if board[pos] == player:
                n = self.neighbors(pos,board)
                for j in n:
                    if board[j] == 'x':
                        b = board[:]
                        b[pos] = 'x'
                        b[j] = player
                        if self.closeMill(j,b):
                            self.generateRemove(b,L,self.W  if player==self.B else self.B)
                        else:
                            L.append(b)
        return L

    def neighbors(self,i,pos):
        switcher = {
            0:[1,3,8],
            1:[0,4,2],
            2:[1,5,13],
            3:[0,4,6,9],
            4:[1,3,5],
            5:[2,4,7,12],
            6:[3,7,10],
            7:[5,6,11],
            8:[0,9,20],
            9:[3,8,10,17],
            10:[6,9,14],
            11:[7,12,16],
            12:[5,11,13,19],
            13:[2,12,22],
            14:[10,15,17],
            15:[14,16,18],
            16:[11,15,19],
            17:[9,14,18,20],
            18:[15,17,19,21],
            19:[12,16,18,22],
            20:[8,17,21],
            21:[18,20,22],
            22:[13,19,21]
        }
        return switcher.get(i, None) 

    def closeMill(self,i,b):
        c = b[i]
        if c != 'x':
            switcher = {
                0: True if ((b[1]==c and b[2]==c) or (b[3]==c and b[6]==c) or (b[8]==c and b[20]==c)) else False,
                1: True if ((b[0]==c and b[2]==c)) else False,
                2: True if ((b[1]==c and b[0]==c) or (b[5]==c and b[7]==c) or (b[13]==c and b[22]==c)) else False,
                3: True if ((b[0]==c and b[6]==c) or (b[4]==c and b[5]==c) or (b[9]==c and b[17]==c)) else False,
                4: True if ((b[3]==c and b[5]==c)) else False,
                5: True if ((b[7]==c and b[2]==c) or (b[3]==c and b[4]==c) or (b[12]==c and b[19]==c)) else False,
                6: True if ((b[0]==c and b[3]==c) or (b[10]==c and b[14]==c)) else False,
                7: True if ((b[2]==c and b[5]==c) or (b[11]==c and b[16]==c)) else False,
                8: True if ((b[0]==c and b[20]==c) or (b[9]==c and b[20]==c)) else False,
                9: True if ((b[8]==c and b[10]==c) or (b[3]==c and b[17]==c)) else False,
                10: True if ((b[8]==c and b[9]==c) or (b[14]==c and b[6]==c)) else False,
                11: True if ((b[7]==c and b[16]==c) or (b[13]==c and b[12]==c)) else False,
                12: True if ((b[11]==c and b[13]==c) or (b[5]==c and b[19]==c)) else False,
                13: True if ((b[11]==c and b[12]==c) or (b[2]==c and b[22]==c)) else False,
                14: True if ((b[6]==c and b[10]==c) or (b[15]==c and b[16]==c) or (b[17]==c and b[20]==c)) else False,
                15: True if ((b[14]==c and b[16]==c) or (b[18]==c and b[21]==c)) else False,
                16: True if ((b[14]==c and b[15]==c) or (b[7]==c and b[11]==c) or (b[19]==c and b[22]==c)) else False,
                17: True if ((b[14]==c and b[20]==c) or (b[3]==c and b[9]==c) or (b[18]==c and b[19]==c)) else False,
                18: True if ((b[17]==c and b[19]==c) or (b[15]==c and b[21]==c)) else False,
                19: True if ((b[17]==c and b[18]==c) or (b[16]==c and b[22]==c) or (b[5]==c and b[12]==c)) else False,
                20: True if ((b[14]==c and b[17]==c) or (b[0]==c and b[8]==c) or (b[21]==c and b[22]==c)) else False,
                21: True if ((b[20]==c and b[22]==c) or (b[15]==c and b[18]==c)) else False,
                22: True if ((b[16]==c and b[19]==c) or (b[13]==c and b[2]==c) or (b[20]==c and b[21]==c)) else False
            }
            # print('returning '+str(switcher.get(i)))
            return switcher.get(i)

    def generateRemove(self,b,L,player):
        # print('generateRemove')
        BinMill_flag = True
        b_copy = []
        for pos in range(23):
            if b[pos] == player:
                if not self.closeMill(pos,b):
                    b_copy = b[:]
                    b_copy[pos] = 'x'
                    L.append(b_copy)
                    BinMill_flag = False
                    break
        if BinMill_flag:
            b_copy = b[:]
            b_copy[pos] = 'x'
            L.append(b_copy)


    def generateHopping(self,board,player):
        # print('GenerateHopping')
        L = []
        for alpha in range(23):
            if board[alpha] == player:
                for beta in range(23):
                    if board[beta] == 'x':
                        b = board[:]
                        b[alpha] = 'x'
                        b[beta] = player
                        if self.closeMill(beta,b):
                            self.generateRemove(b,L,self.W  if player==self.B else self.B)
                        else:
                            L.append(b)
        return L

    def MinMax(self, depth, board, player):        
        self.positions_evaluated_by_static_estimation += 1
        # print player, depth, str(''.join(board)), self.positions_evaluated_by_static_estimation
        if depth == 0:
            return self.statisticEstimation(board)
        else:
            estimate = 1e500
            L = self.generateMovesOpening(board, player)
            for move in L:
                # self.positions_evaluated_by_static_estimation += 1
                child_estimate = self.MaxMin(depth-1,move,self.W  if player==self.B else self.B)
                if child_estimate < estimate:
                    bmove = move[:]                
                    estimate = child_estimate
            return estimate

    def MaxMin(self, depth, board, player):
        self.positions_evaluated_by_static_estimation += 1
        # print player, depth, str(''.join(board)), self.positions_evaluated_by_static_estimation
        if depth == 0:
            return self.statisticEstimation(board)
        else:
            estimate = -1e500
            L = self.generateMovesOpening(board,player)
            for move in L:
                # self.positions_evaluated_by_static_estimation += 1
                child_estimate= self.MinMax(depth-1,move, self.B if player==self.W else self.W)
                if child_estimate > estimate:
                    bmove = move[:]
                    estimate = max(estimate, child_estimate)
                # print player, depth, str(''.join(board)), self.positions_evaluated_by_static_estimation
            self.bestMove = bmove[:]
            return estimate

    def statisticEstimation(self,board):
        # self.positions_evaluated_by_static_estimation += 1
        self.numBlackPieces = 0
        self.numWhitePieces = 0
        for i in range(23):
            if board[i] == 'W':
                self.numWhitePieces += 1
            elif board[i] == 'B':
                self.numBlackPieces += 1
        
        return self.numBlackPieces - self.numWhitePieces

    def __main__(self):
        if len(sys.argv) == 4:
            depth = int(sys.argv[3])
            board = self.readInput(sys.argv[1], sys.argv[2])
            print 'MiniMaxOpeningBlack',str(' '.join(sys.argv[1:]))
            print('Input board: '+str(''.join(board)))

            minEstimate = self.MaxMin(depth,board,'B')

            with open('op_file.txt','w') as opfile:
                opfile.write(str(''.join(self.bestMove)))
            print('Board Position:   '+str(''.join(self.bestMove)))
            print('Positions evaluated by static estimation: '+str(self.positions_evaluated_by_static_estimation))
            print('MINIMAX estimate: '+str(minEstimate))        
        else:
            print("Usage: python NineMensGame.py ip_file.txt op_file.txt 2")


if __name__ == '__main__':
    nm = MiniMaxOpening()
    nm.__main__()